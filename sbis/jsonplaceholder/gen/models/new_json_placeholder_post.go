// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// NewJSONPlaceholderPost Used to create a new post object. Same as JSONPlaceholderPost but without the id attribute.
//
// swagger:model NewJSONPlaceholderPost
type NewJSONPlaceholderPost struct {

	// body
	Body string `json:"body,omitempty"`

	// title
	Title string `json:"title,omitempty"`

	// user Id
	UserID int64 `json:"userId,omitempty"`
}

// Validate validates this new JSON placeholder post
func (m *NewJSONPlaceholderPost) Validate(formats strfmt.Registry) error {
	return nil
}

// ContextValidate validates this new JSON placeholder post based on context it is used
func (m *NewJSONPlaceholderPost) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *NewJSONPlaceholderPost) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *NewJSONPlaceholderPost) UnmarshalBinary(b []byte) error {
	var res NewJSONPlaceholderPost
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
