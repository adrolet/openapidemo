package jphClient

import (
	"testing"
	"fmt"
	"encoding/json"
	"gitlab.com/adrolet/openapidemo/sbis/jsonplaceholder/gen/models"
)

func TestJPHGetPosts(t *testing.T) {

	client := New()

	getResponse, err := GetPosts(client)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	for _, aPost := range getResponse.Payload {

		bytes, _ := json.MarshalIndent(aPost, "", "    ")
		fmt.Printf("get posts = %s\n", string(bytes))
	}
}


func TestJPHPostPost(t *testing.T) {

	client := New()

	postObj := &models.NewJSONPlaceholderPost{}
	postObj.UserID = 1
	postObj.Title = "Good women quote"
	postObj.Body = "If your dream only includes you, it’s too small" // Ava DuVernay

	postResponse, err := PostPost(postObj, client)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	bytes, _ := json.MarshalIndent(postResponse.Payload, "", "    ")
	fmt.Printf("post post = %s\n", string(bytes))
}
