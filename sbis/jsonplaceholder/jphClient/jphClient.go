package jphClient

import (
	"github.com/go-openapi/strfmt"
	httptransport "github.com/go-openapi/runtime/client"
	"gitlab.com/adrolet/openapidemo/sbis/jsonplaceholder/gen/client"
	"gitlab.com/adrolet/openapidemo/sbis/jsonplaceholder/gen/client/operations"
	"gitlab.com/adrolet/openapidemo/sbis/jsonplaceholder/gen/models"
)

func New() *client.Jsonplaceholder {

	jsonPlaceHolderHost := "jsonplaceholder.typicode.com"
	// create the transport
	// server-name/ip, basePath, schemes []string
	transport := httptransport.New(jsonPlaceHolderHost, "/", nil)

	// create the API client, with the transport
	client := client.New(transport, strfmt.Default)

	return client
}

func GetPosts(client *client.Jsonplaceholder) (*operations.GetPostsOK, error){
	params := operations.NewGetPostsParams()
	 ok, err := client.Operations.GetPosts(params)

	if err != nil {
		return nil, err
	}

	return ok, nil
}


// PostPost send a HTTP Post request to create a Post object
// OK, I won't win a prize for the name of this operation!
func PostPost(postObj *models.NewJSONPlaceholderPost, client *client.Jsonplaceholder) (*operations.PostPostCreated, error){
	params := operations.NewPostPostParams()
	params.PostObject = postObj

	ok, err := client.Operations.PostPost(params)

	if err != nil {
		return nil, err
	}

	return ok, nil
}