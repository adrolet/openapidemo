// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the generate command

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
)

// GetPostTitlesHandlerFunc turns a function with the right signature into a get post titles handler
type GetPostTitlesHandlerFunc func(GetPostTitlesParams) middleware.Responder

// Handle executing the request and returning a response
func (fn GetPostTitlesHandlerFunc) Handle(params GetPostTitlesParams) middleware.Responder {
	return fn(params)
}

// GetPostTitlesHandler interface for that can handle valid get post titles params
type GetPostTitlesHandler interface {
	Handle(GetPostTitlesParams) middleware.Responder
}

// NewGetPostTitles creates a new http.Handler for the get post titles operation
func NewGetPostTitles(ctx *middleware.Context, handler GetPostTitlesHandler) *GetPostTitles {
	return &GetPostTitles{Context: ctx, Handler: handler}
}

/*
	GetPostTitles swagger:route GET /place-holder-posts/titles getPostTitles

Retrieve the list of post titles (and id) on the placeholder service
*/
type GetPostTitles struct {
	Context *middleware.Context
	Handler GetPostTitlesHandler
}

func (o *GetPostTitles) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	route, rCtx, _ := o.Context.RouteInfo(r)
	if rCtx != nil {
		*r = *rCtx
	}
	var Params = NewGetPostTitlesParams()
	if err := o.Context.BindValidRequest(r, route, &Params); err != nil { // bind params
		o.Context.Respond(rw, r, route.Produces, route, err)
		return
	}

	res := o.Handler.Handle(Params) // actually handle the request
	o.Context.Respond(rw, r, route.Produces, route, res)

}
