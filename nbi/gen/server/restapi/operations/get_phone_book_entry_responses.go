// Code generated by go-swagger; DO NOT EDIT.

package operations

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"net/http"

	"github.com/go-openapi/runtime"

	"gitlab.com/adrolet/openapidemo/nbi/gen/server/models"
)

// GetPhoneBookEntryOKCode is the HTTP code returned for type GetPhoneBookEntryOK
const GetPhoneBookEntryOKCode int = 200

/*
GetPhoneBookEntryOK Returns a single PhoneBookEntry.

swagger:response getPhoneBookEntryOK
*/
type GetPhoneBookEntryOK struct {

	/*
	  In: Body
	*/
	Payload *models.PhoneBookEntry `json:"body,omitempty"`
}

// NewGetPhoneBookEntryOK creates GetPhoneBookEntryOK with default headers values
func NewGetPhoneBookEntryOK() *GetPhoneBookEntryOK {

	return &GetPhoneBookEntryOK{}
}

// WithPayload adds the payload to the get phone book entry o k response
func (o *GetPhoneBookEntryOK) WithPayload(payload *models.PhoneBookEntry) *GetPhoneBookEntryOK {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get phone book entry o k response
func (o *GetPhoneBookEntryOK) SetPayload(payload *models.PhoneBookEntry) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetPhoneBookEntryOK) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(200)
	if o.Payload != nil {
		payload := o.Payload
		if err := producer.Produce(rw, payload); err != nil {
			panic(err) // let the recovery middleware deal with this
		}
	}
}

// GetPhoneBookEntryNotFoundCode is the HTTP code returned for type GetPhoneBookEntryNotFound
const GetPhoneBookEntryNotFoundCode int = 404

/*
GetPhoneBookEntryNotFound Returns a string indicating that there is no entry for the first-last.

swagger:response getPhoneBookEntryNotFound
*/
type GetPhoneBookEntryNotFound struct {

	/*
	  In: Body
	*/
	Payload string `json:"body,omitempty"`
}

// NewGetPhoneBookEntryNotFound creates GetPhoneBookEntryNotFound with default headers values
func NewGetPhoneBookEntryNotFound() *GetPhoneBookEntryNotFound {

	return &GetPhoneBookEntryNotFound{}
}

// WithPayload adds the payload to the get phone book entry not found response
func (o *GetPhoneBookEntryNotFound) WithPayload(payload string) *GetPhoneBookEntryNotFound {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get phone book entry not found response
func (o *GetPhoneBookEntryNotFound) SetPayload(payload string) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetPhoneBookEntryNotFound) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(404)
	payload := o.Payload
	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}

// GetPhoneBookEntryInternalServerErrorCode is the HTTP code returned for type GetPhoneBookEntryInternalServerError
const GetPhoneBookEntryInternalServerErrorCode int = 500

/*
GetPhoneBookEntryInternalServerError Returns a string indicating what went wrong.

swagger:response getPhoneBookEntryInternalServerError
*/
type GetPhoneBookEntryInternalServerError struct {

	/*
	  In: Body
	*/
	Payload string `json:"body,omitempty"`
}

// NewGetPhoneBookEntryInternalServerError creates GetPhoneBookEntryInternalServerError with default headers values
func NewGetPhoneBookEntryInternalServerError() *GetPhoneBookEntryInternalServerError {

	return &GetPhoneBookEntryInternalServerError{}
}

// WithPayload adds the payload to the get phone book entry internal server error response
func (o *GetPhoneBookEntryInternalServerError) WithPayload(payload string) *GetPhoneBookEntryInternalServerError {
	o.Payload = payload
	return o
}

// SetPayload sets the payload to the get phone book entry internal server error response
func (o *GetPhoneBookEntryInternalServerError) SetPayload(payload string) {
	o.Payload = payload
}

// WriteResponse to the client
func (o *GetPhoneBookEntryInternalServerError) WriteResponse(rw http.ResponseWriter, producer runtime.Producer) {

	rw.WriteHeader(500)
	payload := o.Payload
	if err := producer.Produce(rw, payload); err != nil {
		panic(err) // let the recovery middleware deal with this
	}
}
