// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// PhoneBookEntry phone book entry
// swagger:model PhoneBookEntry
type PhoneBookEntry struct {

	// address
	Address *AddressEntry `json:"Address,omitempty"`

	// first name
	FirstName string `json:"FirstName,omitempty"`

	// last name
	LastName string `json:"LastName,omitempty"`

	// phone number
	PhoneNumber string `json:"PhoneNumber,omitempty"`
}

// Validate validates this phone book entry
func (m *PhoneBookEntry) Validate(formats strfmt.Registry) error {
	var res []error

	if err := m.validateAddress(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

func (m *PhoneBookEntry) validateAddress(formats strfmt.Registry) error {

	if swag.IsZero(m.Address) { // not required
		return nil
	}

	if m.Address != nil {
		if err := m.Address.Validate(formats); err != nil {
			if ve, ok := err.(*errors.Validation); ok {
				return ve.ValidateName("Address")
			}
			return err
		}
	}

	return nil
}

// MarshalBinary interface implementation
func (m *PhoneBookEntry) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *PhoneBookEntry) UnmarshalBinary(b []byte) error {
	var res PhoneBookEntry
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
