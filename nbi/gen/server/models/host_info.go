// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// HostInfo host info
//
// swagger:model HostInfo
type HostInfo struct {

	// architecture
	Architecture string `json:"architecture,omitempty"`

	// host name
	HostName string `json:"host-name,omitempty"`

	// num cpus
	NumCpus int64 `json:"num-cpus,omitempty"`

	// os name
	OsName string `json:"os-name,omitempty"`
}

// Validate validates this host info
func (m *HostInfo) Validate(formats strfmt.Registry) error {
	return nil
}

// ContextValidate validates this host info based on context it is used
func (m *HostInfo) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *HostInfo) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *HostInfo) UnmarshalBinary(b []byte) error {
	var res HostInfo
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
