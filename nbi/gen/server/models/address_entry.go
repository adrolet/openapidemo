// Code generated by go-swagger; DO NOT EDIT.

package models

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"context"

	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
)

// AddressEntry address entry
//
// swagger:model AddressEntry
type AddressEntry struct {

	// city
	City string `json:"City,omitempty"`

	// civic number
	CivicNumber int64 `json:"CivicNumber,omitempty"`

	// postal code
	PostalCode string `json:"PostalCode,omitempty"`

	// state
	State string `json:"State,omitempty"`

	// street
	Street string `json:"Street,omitempty"`

	// zip
	Zip int64 `json:"Zip,omitempty"`
}

// Validate validates this address entry
func (m *AddressEntry) Validate(formats strfmt.Registry) error {
	return nil
}

// ContextValidate validates this address entry based on context it is used
func (m *AddressEntry) ContextValidate(ctx context.Context, formats strfmt.Registry) error {
	return nil
}

// MarshalBinary interface implementation
func (m *AddressEntry) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *AddressEntry) UnmarshalBinary(b []byte) error {
	var res AddressEntry
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}
