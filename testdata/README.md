# testdata Directory

This directory contains files that store json payloads.

These can be used to test PUT and POST APIs.

Feel free to add your own files.
