# Open API Demo Application

A very simple application that provide examples on how REST APIs accepting JSON payload
can be implemented using code generated by go-swagger.

The application presents a server-side North bound API (NBI) as well as a client-side API (SBI).

It is licensed using a MIT-style license. See the file [LICENSE](LICENSE).

# Go Version

This application was tested with Go version 1.13.4.

It should also work on older versions.  
If used on older versions you may need to remove the go.mod (and go.sum) files  
so that Go uses the external libraries from the vendor folder.

---
# Application Overview

![overview diagram](docs/img/openapidemo-overview.png)

The application is organized as a set of layers:
* At the top the NBI accepting external REST calls.
* At the bottom various data providers.  
  OS calls, an internal data store, and a REST client to access another web service.
* In the middle a set of handlers functions.  
  They provide the behavior for the NBI API and act as glue between the top and bottom layers.

The application's NBI supports interaction with three sets of data:
* Information about the host: GET
* Entries in a in-memory phone book: GET (x2), PUT
* Canned-data from the JsonPlaceholder external service: GET (x2), POST

The project structure looks like this (only key directories are shown):
````
.
|-- LICENSE
|-- Makefile
|-- README.md
|-- docs        # Images and slides.
|
|-- handlers    # Glue code connecting the NBI with the bottom layer. The heart of the application.
|
|-- nbi
|   |-- gen                    # Generated code. NBI stubs and data types.
|   |   `-- server
|   |       |-- cmd
|   |       |   `-- openapidemo-server        # The application main function.
|   |       |-- models
|   |       `-- restapi
|   |           `-- configure_openapidemo.go  # File connecting NBI and handlers (the only gen file modified).
|   |-- nbi-swagger.yaml       # The specifications of the app API (NBI generator input).
|   `-- old-configs            # A helper dir to manage re-generated configure_openapidemo.go.
|
|-- sbis
|   `-- jsonplaceholder        # The client to interact with the jsonplaceholder service.
|       |-- gen                # Generated stubs and types to use to build a client.
|       |   |-- client
|       |   `-- models
|       |-- jphClient          # Manually written client code calling the generated code.
|       `-- jsonplaceholder-swagger.yaml      # The specifications of the jsonplaceholder service API.
|
`-- testdata                   # Json payloads that one can use to test the NBI.
    |-- README.md
    |-- good-woman-quote.json
    `-- lennon.json
````

## NBI 

The NBI code is generated by calling the [go-swagger](<https://github.com/go-swagger/go-swagger>) generator with the `nbi-swagger.yaml` API specification. 

The generator generates code in the `nbi/gen/server/models` package to implement each data types defined in the swagger file.

Go-swagger also generates a main function under `nbi/gen/server/cmd/openapidemo-server`.
The name of this directory, is passed in the generation command, and determine the name of the executable. 

The code capturing each HTTP operations of the NBI is generated in the package `nbi/gen/server/restapi `
and its `operations` sub-directory.

The file `configure_<appName>.go` (i.e. configure_openapidemo.go) is generated and is the one that binds 
the code capturing the HTTP operations to the code implementing the application behavior.
  
This is the only generated file that should be modified by the application designer.  
Once generated go-swagger will not generate this file again if called to regenerate the NBI code 
from an updated swagger specifications file.

During the development of the application the NBI specification is likely to evolve (most likely with more operations).  
Therefore from time to time the designer will want to regenerate the nbi code.

A possible way to handle the update of the `configure_*.go` file is to move it outside of the gen directory,  
regenerate the NBI then manually merge the new `configure_*.go` file with the old one that contains some hand-written code. 
 
This is made easier by some simple code in the make file that move the old `configure_*.go` file to the `old-configs` directory.
This mechanism while useful, is far from  perfect. It is left to the reader to develop a production-grade equivalent mechanism!

## SBI Client for JSONPlaceholder

The application also demonstrates how to connect to an external REST service.

For this the application communicates with a server called jsonplaceholder.  
This service offers canned data that can be obtained using HTTP GET, and can fake HTTP PUT and POST.  
See: <https://jsonplaceholder.typicode.com/>.

A part of its API was defined in the file `jsonplaceholder-swagger.yaml` and is used to generate 
the code under `sbis/jsonplaceholder/gen`.

In the case of a client only the `client` `operations` and `models` (data types) packages are needed.

The code under `jpgClient` is hand-written and is used to create HTTP requests toward the jsonplaceholder service.

---
# Building the Application

If you modify the `nbi-swagger.yaml` NBI specification file, you will need to re-generate the NBI stubs and types.  
To do so you must install the `swagger` executable on your development host.

This can be done in many ways (using a Docker image, MacOS Homebrew, or Debian or RPM packages).

As a Go designer you can easily install it from source.  
`$ go get github.com/go-swagger/go-swagger/cmd/swagger`

See these two sites for more information about go-swagger:  
<https://goswagger.io/install.html>  
<https://github.com/go-swagger/go-swagger>

```
# If one of the swagger file is modified
# if nbi-swagger.yaml is modified extra modification will be needed
$ make generate
# or if only the nbi or sbi needs to be regenerated use one of:
    $ make generate-nbi
    $ make generate-sbi

# To compile the application and install the executable under ${GOPATH}/bin
$ make install
```

---
# Running the Application

If you want to skip the installation, (assuming that generation is already done):

``` 
# make run
```

Using an already compiled application executable.

``` 
$ openapidemo-server --port 8888 --host 127.0.0.1
```

Use the host external IP for `--host` if you want to reach the app from another host.  
Use any free port you like for `--port`.

---
# Using the API.

You can use the application by sending various HTTP messages to the server.

One way to do this is to  use `curl`.
You can also use GUI-based tools like [PostMan](https://www.getpostman.com/), or write your own client.

Below is a set of HTTP requests used as examples on how to interact with the application NBI.

The pipe into `python -m json.tool` is optional and only used to pretty-print the json in the resppnse.

## Get Host Info

``` 
$ curl -s http://127.0.0.1:8888/openapidemo/host-info | python -m json.tool
{
    "architecture": "amd64",
    "host-name": "Alain-Drolets-iMac.local",
    "num-cpus": 4,
    "os-name": "darwin"
}
```

The `-s` is to quiet some stats output.    
Feel free to also explore forms like: (good for debugging failing connections)
``` 
$ curl -v http://127.0.0.1:8888/openapidemo/host-info
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to 127.0.0.1 (127.0.0.1) port 8888 (#0)
> GET /openapidemo/host-info HTTP/1.1
> Host: 127.0.0.1:8888
> User-Agent: curl/7.54.0
> Accept: */*
>
< HTTP/1.1 200 OK
< Content-Type: application/json
< Date: Tue, 03 Sep 2019 03:05:14 GMT
< Content-Length: 96
<
{"architecture":"amd64","host-name":"Alain-Drolets-iMac.local","num-cpus":4,"os-name":"darwin"}
* Connection #0 to host 127.0.0.1 left intact
```

## Get Phone Book Content

Get the whole content of the Phone Book as an array of entries.

``` 
$ curl -s http://127.0.0.1:8888/openapidemo/phonebook | python -m json.tool
[
    {
        "Address": {
            "City": "Memphis",
            "CivicNumber": 3764,
            "State": "Tennessee",
            "Street": "Highway 51 South",
            "Zip": 38116
        },
        "FirstName": "Elvis",
        "LastName": "Presley",
        "PhoneNumber": "901-555-5823"
    },
    {
        "Address": {
            "City": "Los Angeles",
            "CivicNumber": 100,
            "State": "California",
            "Street": "Ocean Drive",
            "Zip": 90201
        },
        "FirstName": "Barry",
        "LastName": "White",
        "PhoneNumber": "310-555-9274"
    }
]
```

## Add an Entry to the Phone Book

```
$ curl -s -X PUT  -T 'testdata/lennon.json' --header 'content-type: application/json' "http://127.0.0.1:8888/openapidemo/phonebook" | python -m json.tool
{
    "Address": {
        "City": "Liverpool",
        "CivicNumber": 9,
        "PostalCode": "L15 9HP",
        "State": "North West",
        "Street": "Newcastle Road"
    },
    "FirstName": "John",
    "LastName": "Lennon",
    "PhoneNumber": "0151-555-2845"
}

```
## Get Single Entry from Phone Book

```
$ curl -s http://127.0.0.1:8888/openapidemo/phonebook/Barry/White | python -m json.tool
{
    "Address": {
        "City": "Los Angeles",
        "CivicNumber": 100,
        "State": "California",
        "Street": "Ocean Drive",
        "Zip": 90201
    },
    "FirstName": "Barry",
    "LastName": "White",
    "PhoneNumber": "310-555-9274"
}

# 404 case with verbose output
$ curl -v http://127.0.0.1:8888/openapidemo/phonebook/John/Doe | python -m json.tool
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to 127.0.0.1 (127.0.0.1) port 8888 (#0)
> GET /openapidemo/phonebook/John/Doe HTTP/1.1
> Host: 127.0.0.1:8888
> User-Agent: curl/7.54.0
> Accept: */*
>
< HTTP/1.1 404 Not Found
< Content-Type: application/json
< Date: Tue, 10 Sep 2019 03:46:43 GMT
< Content-Length: 25
<
{ [25 bytes data]
100    25  100    25    0     0   4548      0 --:--:-- --:--:-- --:--:--  5000
* Connection #0 to host 127.0.0.1 left intact
"No entry for John-Doe."
```


## Get Titles from the JSONPlaceholder Service


```
$ curl -s 127.0.0.1:8888/openapidemo/place-holder-posts/titles | python -m json.tool
[
    {
        "id": 1,
        "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit"
    },    {
        "id": 2,
        "title": "qui est esse"
    },
    {
        "id": 3,
        "title": "ea molestias quasi exercitationem repellat qui ipsa sit aut"
    },
...
    {
        "id": 99,
        "title": "temporibus sit alias delectus eligendi possimus magni"
    },
    {
        "id": 100,
        "title": "at nam consequatur ea labore ea harum"
    }
]
```

## Get Entries for a given User from the JSONPlaceholder Service

```
// UserId is an int between 1 and 10.
$ curl -s 127.0.0.1:8888/openapidemo/place-holder-posts/byuser/3 | python -m json.tool
[
    {
        "body": "repellat aliquid praesentium dolorem quo\nsed totam minus non itaque\nnihil labore molestiae sunt dolor eveniet hic recusandae veniam\ntempora et tenetur expedita sunt",
        "id": 21,
        "title": "asperiores ea ipsam voluptatibus modi minima quia sint"
    },
    {
        "body": "eos qui et ipsum ipsam suscipit aut\nsed omnis non odio\nexpedita earum mollitia molestiae aut atque rem suscipit\nnam impedit esse",
        "id": 22,
        "title": "dolor sint quo a velit explicabo quia nam"
    },
...
    {
        "body": "alias dolor cumque\nimpedit blanditiis non eveniet odio maxime\nblanditiis amet eius quis tempora quia autem rem\na provident perspiciatis quia",
        "id": 30,
        "title": "a quo magni similique perferendis"
    }
]
```

## POST an Entry to the JSONPlaceholder Service

```
$ curl -s -X POST  -T 'testdata/good-woman-quote.json' --header 'content-type: application/json' "http://127.0.0.1:8888/openapidemo/place-holder-posts" | python -m json.tool
{
    "body": "If your dream only includes you, it\u2019s too small",
    "id": 101,
    "title": "Good women quote",
    "userId": 11
}
```
